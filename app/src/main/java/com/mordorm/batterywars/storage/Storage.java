package com.mordorm.batterywars.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

public class Storage {

    private static final String PREFS_NAME = "battery_wars";
    private static final String KEY_UUID = "uuid";

    private static Storage sInstance;

    private final Context mContext;

    public static void initialize(Context context) {
        sInstance = new Storage(context);
    }

    public static Storage getInstance() {
        return sInstance;
    }

    private Storage(Context context) {
        mContext = context;
    }

    public String getUuid() {
        SharedPreferences prefs = getPrefs();
        if (prefs.contains(KEY_UUID)) {
            return prefs.getString(KEY_UUID, null);
        } else {
            String uuid = UUID.randomUUID().toString();
            prefs.edit().putString(KEY_UUID, uuid).apply();
            return uuid;
        }

    }

    private SharedPreferences getPrefs() {
        return mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }
}
