package com.mordorm.batterywars.drainers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.provider.Settings;

import java.util.concurrent.TimeUnit;

public class BrightnessDrainer implements Drainer {

    private static final String TAG = "BrightnessDrainer";

    private final long mDurationMs;
    private final ContentResolver mContentResolver;
    private final PowerManager.WakeLock mWakeLock;

    BrightnessDrainer(Context context, long durationMs) {
        mDurationMs = durationMs;
        mContentResolver = context.getContentResolver();

        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, TAG);
    }

    @Override
    public void drain() {
        int originalMode = getBrightnessMode();
        int originalBrightness = getBrightness();

        setBrightnessMode(Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        setBrightness(255);
        mWakeLock.acquire();

        DrainUtil.sleep(mDurationMs);

        setBrightnessMode(originalMode);
        setBrightness(originalBrightness);
        mWakeLock.release();
    }

    private int getBrightnessMode() {
        try {
            return Settings.System.getInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE);
        } catch (Settings.SettingNotFoundException e) {
            return Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        }
    }

    private void setBrightnessMode(int mode) {
        Settings.System.putInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, mode);
    }

    private int getBrightness() {
        try {
            return Settings.System.getInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            return 1;
        }
    }

    private void setBrightness(int brightness) {
        Settings.System.putInt(mContentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
    }

    static class Factory implements Drainer.Factory<BrightnessDrainer> {

        @Override
        public BrightnessDrainer build(Context context, Intent intent) {
            return new BrightnessDrainer(
                    context,
                    intent.getLongExtra(DrainerService.KEY_DURATION, TimeUnit.SECONDS.toMillis(5)));
        }
    }
}
