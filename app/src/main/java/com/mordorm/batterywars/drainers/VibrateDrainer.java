package com.mordorm.batterywars.drainers;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

import java.util.concurrent.TimeUnit;

class VibrateDrainer implements Drainer {

    private static final long VIBRATE_INTERVAL_MS = 100;

    private final long mDurationMs;
    private final Vibrator mVibrator;

    VibrateDrainer(Context context, long durationMs) {
        mDurationMs = durationMs;
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public void drain() {
        // Background the app will interrupt a single long vibrate, so we do many short ones
        long timeLeft = mDurationMs;
        while (timeLeft > 0) {
            mVibrator.vibrate(VIBRATE_INTERVAL_MS);
            DrainUtil.sleep(VIBRATE_INTERVAL_MS);
            timeLeft -= VIBRATE_INTERVAL_MS;
        }
    }

    static class Factory implements Drainer.Factory<VibrateDrainer> {

        @Override
        public VibrateDrainer build(Context context, Intent intent) {
            return new VibrateDrainer(
                    context,
                    intent.getLongExtra(DrainerService.KEY_DURATION, TimeUnit.SECONDS.toMillis(5)));
        }
    }
}
