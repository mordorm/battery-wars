package com.mordorm.batterywars.drainers;

class DrainUtil {

    static void sleep(long timeMs) {
        try {
            Thread.sleep(timeMs);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();;
        }
    }
}
