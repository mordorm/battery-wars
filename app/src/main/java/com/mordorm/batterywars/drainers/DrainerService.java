package com.mordorm.batterywars.drainers;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

public class DrainerService extends IntentService {

    private static final String TAG = "DrainerService";

    public static final String KEY_DURATION = "duration";

    private final Map<String, Drainer.Factory> mDrainerMap;

    // Instance initializer
    {
        mDrainerMap = new HashMap<>();
        mDrainerMap.put(DrainerConstants.ACTION_VIBRATE, new VibrateDrainer.Factory());
        mDrainerMap.put(DrainerConstants.ACTION_BRIGHTNESS, new BrightnessDrainer.Factory());
    }

    public DrainerService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }

        // Show foreground notification
        Notification.Builder builder = new Notification.Builder(getBaseContext())
                .setContentTitle("Your Battery is Being Drained!")
                .setContentText("Sucks for you!")
                .setProgress(0, 0, true);
        startForeground(1, builder.build());

        // Do the work, then stop the foreground notification
        try {
            Drainer.Factory drainerFactory = mDrainerMap.get(intent.getAction());
            if (drainerFactory == null) {
                throw new IllegalArgumentException("The intent's action matches no known Drainers. Maybe you forgot to " +
                        "register its factory in the map?");
            }
            drainerFactory.build(getBaseContext(), intent).drain();
        } finally {
            stopForeground(true);
        }
    }
}
