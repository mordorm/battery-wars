package com.mordorm.batterywars.drainers;

import android.content.Context;
import android.content.Intent;

interface Drainer {

    void drain();

    interface Factory<T extends Drainer> {
        T build(Context context, Intent intent);
    }
}
