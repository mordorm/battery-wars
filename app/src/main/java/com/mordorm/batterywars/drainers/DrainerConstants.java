package com.mordorm.batterywars.drainers;

public class DrainerConstants {
    public static final String ACTION_VIBRATE = "vibrate";
    public static final String ACTION_BRIGHTNESS = "brightness";
}
