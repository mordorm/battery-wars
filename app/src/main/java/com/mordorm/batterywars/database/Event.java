package com.mordorm.batterywars.database;

import java.util.HashMap;
import java.util.Map;

public class Event {

    private static final String KEY_SENDER = "sender";
    private static final String KEY_ACTION = "action";
    private static final String KEY_DURATION = "duration";

    private final String mSenderId;
    private final String mAction;
    private final long mDurationMs;

    public Event(String senderId, String action, long durationMs) {
        mSenderId = senderId;
        mAction = action;
        mDurationMs = durationMs;
    }

    public static Event from(Map<String, String> map) {
        return new Event(
                map.get(KEY_SENDER),
                map.get(KEY_ACTION),
                Long.parseLong(map.get(KEY_DURATION))
        );
    }

    public String getSenderId() {
        return mSenderId;
    }

    public String getAction() {
        return mAction;
    }

    public long getDurationMs() {
        return mDurationMs;
    }

    public Map<String, String> toMap() {
        Map<String, String> out = new HashMap<>();
        out.put(KEY_SENDER, mSenderId);
        out.put(KEY_ACTION, mAction);
        out.put(KEY_DURATION, String.valueOf(mDurationMs));
        // TODO: Include target
        return out;
    }
}
