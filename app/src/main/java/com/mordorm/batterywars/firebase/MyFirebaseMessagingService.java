package com.mordorm.batterywars.firebase;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mordorm.batterywars.database.Event;
import com.mordorm.batterywars.drainers.DrainerService;
import com.mordorm.batterywars.storage.Storage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e(TAG, "Message Received!");
        Log.e(TAG, remoteMessage.getData().toString());

        Event event = Event.from(remoteMessage.getData());

        // If we sent it, we don't want to run it
        // TODO: Remove this once we actually send it only to the intended target
//        if (Storage.getInstance().getUuid().equals(event.getSenderId())) {
//            return;
//        }

        // Run the Drainer
        Intent intent = new Intent(getBaseContext(), DrainerService.class);
        intent.setAction(event.getAction());
        intent.putExtra(DrainerService.KEY_DURATION, event.getDurationMs());
        startService(intent);
    }
}
