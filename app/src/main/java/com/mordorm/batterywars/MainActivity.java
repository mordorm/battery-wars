package com.mordorm.batterywars;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.greysonparrelli.permiso.Permiso;
import com.greysonparrelli.permiso.PermisoActivity;
import com.mordorm.batterywars.database.Event;
import com.mordorm.batterywars.drainers.DrainerConstants;
import com.mordorm.batterywars.drainers.DrainerService;
import com.mordorm.batterywars.storage.Storage;

import java.util.concurrent.TimeUnit;

public class MainActivity extends PermisoActivity {

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Check auth
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            LoginActivity.launch(this);
        }

        // Check permissions
        if (Build.VERSION.SDK_INT < 23 || Settings.System.canWrite(this)) {
            requestPermissions();
        } else {
            // WRITE_SETTINGS is a special permission starting in API 23 that users need to enable in their settings
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:com.mordorm.batterywars"));
            startActivity(intent);
        }
    }

    private void requestPermissions() {
        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
            @Override
            public void onPermissionResult(Permiso.ResultSet resultSet) {
                if (resultSet.areAllPermissionsGranted()) {
                    onPermissionsGranted();
                } else {
                    Toast.makeText(MainActivity.this, "Come back when you're ready to play. Loser.", Toast.LENGTH_LONG).show();
                    MainActivity.this.finish();
                }
            }

            @Override
            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                Permiso.getInstance().showRationaleInDialog("Give us the flippin' permissions", "Please?", "Thanks", callback);
            }
        }, Manifest.permission.VIBRATE, Manifest.permission.WAKE_LOCK);
    }

    private void onPermissionsGranted() {
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Add click listener
        findViewById(R.id.vibrate_button).setOnClickListener(new DrainerClickListener(mDatabase, DrainerConstants.ACTION_VIBRATE));
        findViewById(R.id.brightness_button).setOnClickListener(new DrainerClickListener(mDatabase, DrainerConstants.ACTION_BRIGHTNESS));
    }

    private static class DrainerClickListener implements View.OnClickListener {

        private final DatabaseReference mDatabase;
        private final String mAction;

        public DrainerClickListener(DatabaseReference database, String action) {
            mDatabase = database;
            mAction = action;
        }

        @Override
        public void onClick(View view) {
            Event event = new Event(
                    Storage.getInstance().getUuid(),
                    mAction,
                    TimeUnit.SECONDS.toMillis(5));
            mDatabase.child("events").push().setValue(event.toMap());
        }
    }
}
