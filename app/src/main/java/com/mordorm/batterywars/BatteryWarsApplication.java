package com.mordorm.batterywars;

import android.app.Application;

import com.mordorm.batterywars.storage.Storage;

public class BatteryWarsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Storage.initialize(this);
    }
}
